'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Application configuration
 */
module.exports = {
    debug: true,

    siteName: 'Iniv Js',

    adminUri: 'backend',
  
    
    locals: ['en','hi'],
    
    
    defaultLocal: 'hi',

    providers: [
        'Games/Poker/ServiceProvider'
    ],
    plugins: [
	'@inivjs/master'
    ]
}
