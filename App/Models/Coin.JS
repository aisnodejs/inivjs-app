const Model = load('Iniv/Database/Model')
class Coin extends Model {
	/**
	 * Required function
	 */
	static identity() {
		return 'coin'
	}
	static primaryKey() {
		return 'id'
	}
	static schema() {
		return true
	}
	static attributes() {
		return {
			detail: {
				type: 'string',
				required: true
			},

		};
	}
}

module.exports = Coin