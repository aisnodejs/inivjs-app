'use strict'

class SocketController {

  constructor({socket, request, auth}){
    this.socket = socket
    this.request = request
    this.auth = auth
  }
  
  onMessage(message){
    this.socket.toEveryone().emit('message',message)
  }
  onErrorTest(data){
    console.log(message)
    this.socket.toEveryone().emit('message',data)
    return data
  }
  disconnected(socket){
    console.log('connected',socket.id)
  }
}

module.exports = SocketController