'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */


class LogUrlToConsole {
    static inject() {
        return ['Iniv/Config']
    }
    constructor(Config) {
        this.Config = Config
    }
    async handle({ request }, next) {
        Logger.info(request.method(), request.url())

        console.log(this.Config.get('http'))
        
        await next()
    }
}

module.exports = LogUrlToConsole
