const { validate } = load('Iniv/Validator')

class LoginController {
    async getLogin({ request, response, view, session }) {
        return view.render('frontend/auth/login')
    }
    async postLogin({ request, response, auth, session }) {
        // validation rules
        const rules = {
            email: 'required|email',
            password: 'required'
        }
        const validation = await validate(request.all(), rules)
        // validation check
        if (validation.fails()) {
            session
                .withErrors(validation.messages())
                .flashExcept(['password'])

            return response.redirect('back')
        }

        const { email, password } = request.all()
        await auth.remember(true).attempt(email, password)
        session.flash({'success': 'Player successfully logged in.'})
        return response.redirect('/home')
    }

    async getProfile({ auth, view }) {
        return view.render('frontend/auth/profile',{user:auth.user})
    }
    
    async postProfile({ auth, view }) {
        return view.render('frontend/auth/profile',{user:auth.user})
    }

    async getSetting({ request, response, view, session }) {
        return view.render('frontend/auth/setting')
    }

    async getLogout ({response, auth, session}){
        await auth.logout()
        session.flash({'success': 'Player successfully logged out.'})
        return response.redirect('/auth/login')
    }
}

module.exports = LoginController