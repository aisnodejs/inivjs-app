'use strict'

class ChatController {

  constructor({ socket, request, auth, session }) {
    this.socket = socket
    this.request = request
    this.auth = auth
    this.session = session
    console.log('new connection received', socket.id, 'Chat Controller')
  }

  // implimenting middleware for socket event
  register() {
    return {
      'onMessage': ['socketAuth']
    }
  }

  onMessage(data, player) {
    this.socket.toEveryone().emit('message', 'ghfhg')
  }
  onMessageTest(data, player) {
    console.log(data)
    this.socket.toEveryone().emit('message', 'ghfhg')
  }
  onErrorTest(data) {
    console.log(this.session.get('hi'))
    this.session.put('hi','bye')
    // this.socket.toEveryone().emit('message', data)
    return data
  }
  disconnected(socket) {
    console.log('connected', socket.id)
  }
}

module.exports = ChatController