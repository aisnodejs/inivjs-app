/**
 * Default model settings
 */

module.exports = {
    
    
      /***************************************************************************
      *                                                                          *
      * Whether the `.create()` and `.update()` model methods should ignore      *
      * (and refuse to persist) unrecognized data-- i.e. properties other than   *
      * those explicitly defined by attributes in the model definition.          *
      *                                                                          *
      * To ease future maintenance of your code base, it is usually a good idea  *
      * to set this to `true`.                                                   *
      *                                                                          *
      * > Note that `schema: false` is not supported by every database.          *
      * > For example, if you are using a SQL database, then relevant models     *
      * > are always effectively `schema: true`.  And if no `schema` setting is  *
      * > provided whatsoever, the behavior is left up to the database adapter.  *
      * >                                                                        *
      * > For more info, see:                                                    *
      * > https://sailsjs.com/docs/concepts/orm/model-settings#?schema           *
      *                                                                          *
      ***************************************************************************/
    
      schema: true,
    
    
    
      /***************************************************************************
      *                                                                          *
      * Base attributes that are included in all of your models by default.      *
      * By convention, this is your primary key attribute (`id`), as well as two *
      * other timestamp attributes for tracking when records were last created   *
      * or updated.                                                              *
      *                                                                          *
      * > For more info, see:                                                    *
      * > https://sailsjs.com/docs/concepts/orm/model-settings#?attributes       *
      *                                                                          *
      ***************************************************************************/
    
      attributes: {
        createdAt: { type: 'number', autoCreatedAt: true, },
        updatedAt: { type: 'number', autoUpdatedAt: true, },
        id: { type: 'string', columnName: '_id' },
        //--------------------------------------------------------------------------
        // id: { type: 'number', autoIncrement: true, },
        //  /\   Using MongoDB?
        //  ||   Replace `id` above with this instead:
        //
        // ```
        // id: { type: 'string', columnName: '_id' },
        // ```
        //--------------------------------------------------------------------------
      },
    }