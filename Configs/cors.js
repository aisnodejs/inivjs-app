'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Cors configuration
 */
module.exports = {
    enable: true,
    origins: [
        'http://localhost',
    ],
    methods: [
        'GET', 
        'PUT', 
        'POST'
    ]
}