'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * GenericError Class
 */
const Exception = load('Iniv/Error/Exception')

Exception.handle('InvalidSessionException', async (error, { request, response }) => {
    return response.redirect('/auth/login')
})
