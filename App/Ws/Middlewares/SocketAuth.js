'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */


class SocketAuth {
    static inject() {
        return ['Iniv/Config']
    }
    constructor(Config) {
        this.Config = Config
    }
    async handle({request,data}, next) {
        data.ram = 'shyam'
        Logger.info('Testing log')
        return {
            message: 'INIVILID_TOKEN',
            result : await load('App/Models/User').find(),
            statusCode : 401
        }
        return await next()
    }
}

module.exports = SocketAuth
