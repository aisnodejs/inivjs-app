'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Mail configuration
 */
module.exports = {
    smtp: {
        host: 'smtp.example.com',
        port: 587,
        secure: false, // upgrade later with STARTTLS
        auth: {
            user: 'username',
            pass: 'password'
        }
    },
    options: {

    }
}