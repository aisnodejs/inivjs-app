'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * ServiceProvider Class
 */
class ServiceProvider {


  /**
   *
   * @method register
   *
   * @return {void}
   */
  register() {

  }

  /**
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {

  }
}
module.exports = ServiceProvider