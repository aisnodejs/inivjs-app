class RegisterController {
    async getRegister({request, response, view }){
        let users = await load('App/Models/User').find({id:'59e087a9e3be1c781ccb5e9c'});

        return view.render('frontend/auth/register')
    }
    async postRegister({request, response, session }){
        let user = request.all()
        user.password = await load('Iniv/Hash').make(user.password)
        let users = await load('App/Models/User').create(request.all());
        session.flash({'success':"User register successfully. Please login"})
        session.commit()
        return response.redirect('back')
    }
}

module.exports = RegisterController