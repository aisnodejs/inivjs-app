class UserValidator {

    get rules() {
        return { 
            name: 'required',
            email: 'required|email',
            password: 'required',
            retype_password: 'required|same:password' 
        }
    }
    get messages () {
        return {
            'name.required':'some text heare'
        }
    }
    get sanitizationRules() {
        // sanitize data before validation
    }

}

module.exports = UserValidator