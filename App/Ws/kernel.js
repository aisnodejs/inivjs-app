const Ws = load('Iniv/Ws')

const globalMiddleware = [
  'Iniv/Middleware/Session',
  'Iniv/Middleware/AuthInit'
]

const namedMiddleware = {
  auth: 'Iniv/Middleware/Auth',
  socketAuth: 'App/Ws/Middlewares/SocketAuth'
}

Ws.global(globalMiddleware)
Ws.named(namedMiddleware)