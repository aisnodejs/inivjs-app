class Listeners {
    register (){
        this.events.set('TestEvent', ['TestEvent', 'TestEvent'])
        this.events.set("TestEvent1", (data) => {
            console.log('TestEvent1 received')
        })
    }
}

module.exports = Listeners