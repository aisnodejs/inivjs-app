'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Database configuration
 */


module.exports = {
	/***************************************************************************
      *                                                                          *
      * How and whether Sails will attempt to automatically rebuild the          *
      * tables/collections/etc. in your schema.                                  *
      *                                                                          *
      ***************************************************************************/

	migrate : 'alter', // drop, alter, create, safe

	adapters: {
		'sails-mongo': require('sails-mongo')
	},

	datastore: 'mongo',
	datastores : {

		mongo: {
			adapter: 'sails-mongo',
			host: 'localhost',
			user: 'root',
			database: 'inivjs'
		}
	}
}