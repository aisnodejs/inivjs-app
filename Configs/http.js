'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Http configuration
 */
module.exports = {
	etag : true,
	port : '8000',
	/* List the folders to add in static file load */
	staticFiles : [
		'Assets',
		'Resources',
		'public'
	]
		
}