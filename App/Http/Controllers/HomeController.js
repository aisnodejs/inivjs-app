class HomeController {
    async home({request, response, view}){
        return view.render('home')
    }
    async data ({request, response, view}){
        return await load('App/Models/User').find()
    }
}

module.exports = HomeController