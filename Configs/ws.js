'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Socket configuration
 */
module.exports = {
	/*
	|--------------------------------------------------------------------------
	| Using The Same Http Server
	|--------------------------------------------------------------------------
	|
	| Using the same http server for websockets too. If you set this to false
	| make sure to attach a custom http server using Ws.attach()
	|
	*/
	useHttpServer: true,
	useUws: false,
		/*
	|--------------------------------------------------------------------------
	| Changing ping pong
	|--------------------------------------------------------------------------
	|
	|
	*/
	pingInterval: 10000,
	pingTimeout: 5000,
	buildClient: false
}
