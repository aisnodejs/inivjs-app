'use strict'

class SocketController {

  constructor({socket, request, auth}){
    this.socket = socket
    this.request = request
    this.auth = auth
  }
  
  onRegister(data){
    this.socket.toEveryone().emit('message',message)
  }
  onLogin(data){
    this.socket.toMe().emit('message',data)
    return data
  }
  disconnected(socket){
    console.log('connected',socket.id)
  }
}

module.exports = SocketController