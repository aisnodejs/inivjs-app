'use strict'

const Router = load('Iniv/Router')

Router.get('/', ({ request, response, view }) => {
    return view.render('welcome')
});

Router.get('/home', 'HomeController.home').middleware('auth');

/**
 * Auth Login Routes
 */
Router.get('/auth/login', 'Auth/LoginController.getLogin');
Router.post('/auth/login', 'Auth/LoginController.postLogin');

/**
 * Auth Login Routes
 */
Router.get('/auth/profile', 'Auth/LoginController.getProfile').middleware('auth');
Router.post('/auth/profile', 'Auth/LoginController.postProfile').middleware('auth');

/**
 * Auth setting Routes
 */
Router.get('/auth/setting', 'Auth/LoginController.getSetting').middleware('auth');
Router.post('/auth/setting', 'Auth/LoginController.postSetting').middleware('auth');

/**
 * Auth Logout Routes
 */
Router.get('/auth/logout', 'Auth/LoginController.getLogout').middleware('auth');

/**
 * Auth Register Routes
 */
Router.get('/auth/register', 'Auth/RegisterController.getRegister');
Router.post('/auth/register', 'Auth/RegisterController.postRegister').validator('User');

/**
 * Auth Register Routes
 */
Router.get('/auth/forget', 'Auth/ForgetController.getForget');
Router.post('/auth/forget', 'Auth/ForgetController.postForget');


// Adding simple route
Router.get('/welcome', ({ request, response, view }) => {
    // return view.render('newwelcome')
    return {hello:'world'}
});

Router.get('/data', 'HomeController.data');

