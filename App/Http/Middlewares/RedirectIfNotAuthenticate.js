'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */


class RedirectIfNotAuthenticate {
    static inject() {
        return ['Iniv/Config']
    }
    constructor(Config) {
        this.Config = Config
    }
    async handle({ request, auth, response }, next) {
        if(auth.check()){
            return response.redirect('/backend')
        }
        await next()
    }
}

module.exports = RedirectIfNotAuthenticate
