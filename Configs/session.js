'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Sessions configuration
 */
module.exports = {
    /*
    |--------------------------------------------------------------------------
    | Encryption secret
    |--------------------------------------------------------------------------
    |
    | This secret key is used by the NodeSession to encrypt session and sign cookies
    | etc. This should be set to a random, 32 character string, otherwise these
    | encrypted strings will not be safe.
    |
    */

    'secret': 'Q3UBzdH9GEfiRCTKbi5MTPyChpzXLsTe',

    /*
     |--------------------------------------------------------------------------
     | Default Session Driver
     |--------------------------------------------------------------------------
     |
     | This option controls the default session "driver" that will be used on
     | requests. By default, NodeSession will use the lightweight file driver but
     | you may specify any of the other wonderful drivers provided here.
     |
     | Supported: "memory", "file", "database"
     |
     */

    'driver': 'file',

    /*
     |--------------------------------------------------------------------------
     | Session Lifetime
     |--------------------------------------------------------------------------
     |
     | Here you may specify the number of milli seconds that you wish the session
     | to be allowed to remain idle before it expires. If you want them
     | to immediately expire on the browser closing, set that option.
     |
     | Default lifetime value: 300000
     | Default expireOnClose value: false
     |
     */

    'lifetime': 300000, // 5 minutes

    'expireOnClose': false,



    /*
     |--------------------------------------------------------------------------
     | Session File Location
     |--------------------------------------------------------------------------
     |
     | When using the file session driver, we need a location where session
     | files may be stored. By default NodeSession will use the location shown here
     | but a different location may be specified. This is only needed for
     | file sessions.
     |
     */

    'files': process.cwd() + '/sessions',

    /*
     |--------------------------------------------------------------------------
     | Session Database Connection
     |--------------------------------------------------------------------------
     |
     | When using the "database" session driver, you may specify a connection that
     | should be used to manage these sessions.
     |
     | NodeSession uses Nodejs Waterline module for database interactions, hence
     | it supports all databases supported by waterline. Before you specify a
     | waterline adapter with your connection make sure that you have installed
     | it in your application.
     |
     | For example before using sail-mong adapter
     | Run:
     | npm install sails-mongo
     |
     */

    'connection': {
        'adapter': 'sails-mongo',
        'host': 'localhost',
        'port': 27017,
        'user': 'tron',
        'password': '',
        'database': 'tron'
    },

    /*
     |--------------------------------------------------------------------------
     | Session Database Table
     |--------------------------------------------------------------------------
     |
     | When using the "database" session driver, you may specify the table we
     | should use to manage the sessions. Of course, NodeSession uses `sessions`
     | by default; however, you are free to change this as needed.
     |
     */

    'table': 'sessions',

    /*
     |--------------------------------------------------------------------------
     | Session Sweeping Lottery
     |--------------------------------------------------------------------------
     |
     | Some session drivers must manually sweep their storage location to get
     | rid of old sessions from storage. Here are the chances that it will
     | happen on a given request. By default, the odds are 2 out of 100.
     |
     */

    'lottery': [2, 100],

    /*
     |--------------------------------------------------------------------------
     | Session Cookie Name
     |--------------------------------------------------------------------------
     |
     | Here you may change the name of the cookie used to identify a session
     | instance by ID. The name specified here will get used every time a
     | new session cookie is created by the NodeSession for every driver.
     |
     | default: 'iniv_session'
     |
     */

    'cookieName': 'iniv_session',
    
    'cookie': {
        httpOnly: true,
        sameSite: false
    },
    /*
     |--------------------------------------------------------------------------
     | Session Cookie Path
     |--------------------------------------------------------------------------
     |
     | The session cookie path determines the path for which the cookie will
     | be regarded as available. Typically, this will be the root path of
     | your application but you are free to change this when necessary.
     |
     | default: '/'
     |
     */

    'path': '/',

    /*
     |--------------------------------------------------------------------------
     | Session Cookie Domain
     |--------------------------------------------------------------------------
     |
     | Here you may change the domain of the cookie used to identify a session
     | in your application. This will determine which domains the cookie is
     | available to in your application.
     |
     | default: null
     |
     */

    'domain': null,

    /*
     |--------------------------------------------------------------------------
     | HTTPS Only Cookies
     |--------------------------------------------------------------------------
     |
     | By setting this option to true, session cookies will only be sent back
     | to the server if the browser has a HTTPS connection. This will keep
     | the cookie from being sent to you if it can not be done securely.
     |
     | default: false
     |
     */

    'secure': false,

    /**
    |-----------------------------------------------------------------------------
    | Encrypt session
    |-----------------------------------------------------------------------------
    |
    | If you need all stored session data to be encrypted, set the encrypt
    | configuration option to true.
    |
    | default: false
    |
    */

    'encrypt': false
}